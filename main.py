#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 10:42:00 2018

@author: dennis
"""

import os
import pos_tagger as p
from conllu import parse
import json

def main():
    
    path = os.path.dirname(os.path.abspath(__name__))
    
    while(True):
        
        print("Quale azione vuoi effettuare?\n")
        print("\t1 - Calcola le probabilità per Viterbi")
        print("\t2 - Calcola massime occorrenze per Baseline")
        print("\t3 - Valuta accuratezza sul test set")
        print("\t4 - Traduci le frasi di prova")
        print("\t5 - Termina programma")
        CHOISE = input(">>> ")
        
        if int(CHOISE) == 1 or int(CHOISE) == 2:
            
            print("\nCarico il training set...")       
            with open(path+"/UD_Italian/all-IT-ud21-train.conllu", 'r',encoding="utf8",errors='ignore') as file:
              data = file.read()
              ud_train = parse(data)
              
        if int(CHOISE) == 1:    
                  
            print("Sto computando le probabilità...\n")
            states = p.searchState(ud_train)
            emission_probability = p.emission_probability(ud_train,states)
            transition_probability = p.transition_probability(ud_train,states)
            start_probability = p.ProbStart(ud_train,states)
                
        elif int(CHOISE) == 2:
            
            print("Sto computando le massime occorrenze...\n")
            words_tags = p.maxTag(ud_train)
    
        elif int(CHOISE) == 3:
            
            print("\nCarico il test set...")
            with open(path+"/UD_Italian/all-IT-ud21-test.conllu", 'r',encoding="utf8",errors='ignore') as file:
                data = file.read()  
                ud_test = parse(data)
                
            correct_viterbi = []
            correct_baseline = []
            
            print("Sto valutando...\n")
            
            for phrase in ud_test: 
                obs = [phrase[i]["form"] for i in range(len(phrase)) if phrase[i]["upostag"] != "_"]
                tag_test = [phrase[i]["upostag"] for i in range(len(phrase)) if phrase[i]["upostag"] != "_"]
                    
                ep_smoothed = p.smoothing(obs,emission_probability,states)
                _, tag_viterbi = p.viterbi(obs,ep_smoothed,transition_probability,start_probability,states)
                
                tag_baseline = p.baseline(obs,states,words_tags)
                
                for x,y,z in zip(tag_test,tag_viterbi,tag_baseline):
                    if x == y:
                        correct_viterbi.append(True)         
                    else:
                        correct_viterbi.append(False)
                    if x == z:
                        correct_baseline.append(True)
                    else:
                        correct_baseline.append(False)
                
            accurancy_baseline = round(correct_baseline.count(True)/ len(correct_baseline) * 100,2)
            accurancy_viterbi = round(correct_viterbi.count(True)/ len(correct_viterbi) * 100,2)
            print("accuratezza baseline:" + str(accurancy_baseline)+"%")
            print("accuratezza viterbi:" + str(accurancy_viterbi)+"%")
                    
        elif int(CHOISE) == 4:
            
            with open('dictionary.json') as data:
                dictionary = json.load(data)
                
            phrase = ["È la spada laser di tuo padre .",
                      "Ha fatto una mossa leale .",
                      "Gli ultimi avanzi di la vecchia Repubblica sono stati spazzati via ."]
            
            tag_list = []
            l = [] 
            directENG = []
            
            for obs in phrase:
                obs = obs.split()
                ep_smoothed = p.smoothing(obs,emission_probability,states)
                _, tag_viterbi = p.viterbi(obs,ep_smoothed,transition_probability,start_probability,states)
                tag_list.append((obs,tag_viterbi))
                
            for frase in tag_list: 
                for word,tag in zip(frase[0],frase[1]):
                    l.append((dictionary[word],tag))
                directENG.append(l)
                l = []
                
            #####################REGOLE REORDERING####################
            
            
            for phrase,i in zip(directENG,range(len(directENG))):
                tags = [x[1] for x in directENG[i]]
                
                i1 = -1
                i2 = -1
                
                #REGOLA 1: inversione NOUN e ADJ
                if "NOUN" in tags and "ADJ" in tags and (tags.index("ADJ") - tags.index("NOUN") == 1):
                    i1 = tags.index("NOUN")
                    i2 = tags.index("ADJ")
                    directENG[i][i2], directENG[i][i1] = directENG[i][i1], directENG[i][i2]
                    tags = [x[1] for x in directENG[i]]
                    
                #REGOLA 2: cambio NOUN NOUN (saber light)
                if "NOUN" in tags and (tags[tags.index("NOUN") + 1] == "NOUN"): 
                    i1 = tags.index("NOUN")
                    i2 = tags.index("NOUN") + 1
                    directENG[i][i2], directENG[i][i1] = directENG[i][i1], directENG[i][i2]
                    
                #REGOLA 3: unione nomi composti (lightsaber)
                if "NOUN" in directENG[i][i1] and "NOUN" in directENG[i][i2]:
                    temp = (directENG[i][i1][0] + directENG[i][i2][0], directENG[i][i1][1])
                    directENG[i][i1] = temp
                    del directENG[i][i2]
                    tags = [x[1] for x in directENG[i]]
                    
                #REGOLA 4: genitivo sassone
                if "NOUN" in tags and (directENG[i][tags.index("NOUN") + 1][0] == "of"):
                    rule4 = False
                    i1 = tags.index("NOUN") + 1
                    for j in range(i1 + 1,len(tags)):
                        if tags[j] == "ADJ": break
                        if tags[j] == "NOUN": 
                            i2 = j
                            rule4 = True
                            break
                    if rule4:
                        directENG[i][i1 - 1], directENG[i][i2] = directENG[i][i2], directENG[i][i1 - 1]
                        directENG[i][i1 - 1] = (directENG[i][i1 - 1][0]+"'s",directENG[i][i1 - 1][1])
                        for j in range(i1 + 1, i2):
                            directENG[i][i1 - 1], directENG[i][j] = directENG[i][j], directENG[i][i1 - 1]               
                        del directENG[i][i1] #eliminazione of
                        tags = [x[1] for x in directENG[i]]
                        
                #REGOLA 5: inizio frase con un ausiliare
                if "AUX" in tags[0] and directENG[i][0][0] == 'Is':
                     directENG[i][0] = ("It's",directENG[i][0][1])
                elif "AUX" in tags[0] and directENG[i][0][0] == 'Has':
                     directENG[i][0] = ("He",directENG[i][0][1])
                
                #REGOLA 6: eliminazione di uno di due articoli consecutivi
                if "DET" in tags and (tags[tags.index("DET") + 1] == "DET"): 
                    del directENG[i][tags.index("DET")] #eliminazione the
                    tags = [x[1] for x in directENG[i]]
            
            translation = ""
            
            for phrase in directENG:
                for word in phrase:
                  translation += word[0] + " "
                print("\n"+translation+"\n")
                translation = ""

                    
        elif int(CHOISE) == 5: break

if __name__ == "__main__":
    main()
    