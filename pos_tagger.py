#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 09:21:09 2018

@author: dennis
"""

"""
Cerca tutti i tag possibili del treebank
"""
def searchState(treebank):
    
    states = {}

    for phrase in treebank:
        for word in phrase:
            tag = word["upostag"]
            if tag == "_": continue
        
            if tag not in states:
                states[tag] = 1
            else:
                states[tag] += 1
    return list(states.keys())

"""
Calcola le probabilità che la frase inizi con tale tag
"""
def ProbStart(treebank,states):
    
    startProbability = {state: 0 for state in states}
    for phrase in treebank:
        if phrase[0]['upostag'] == "_": continue
        startProbability[phrase[0]['upostag']] += 1

    for key in startProbability:
       startProbability[key] /= len(treebank)
       

    return startProbability

"""
Calcola le probabilità di emissione P(t,w) per l'algoritmo di viterbi
"""
def emission_probability(treebank,states):
    
    prob = {}
    prob_occ = {x: 0 for x in states}
    
    for phrase in treebank:
        for word in phrase:
            form = word["form"]
            if word["upostag"] == "_": continue
            for tag in states:
                prob[(tag,form)] = 0
    
    for phrase in treebank:
        for word in phrase:
            if word["upostag"] == "_": continue            
            prob[(word['upostag'], word['form'])] += 1
            prob_occ[word['upostag']] += 1
            
    for (t, w) in prob:
        prob[(t, w)] /= prob_occ[t]
        
    return prob

"""
Calcola le probabilità di transizione  P(t-1,t) per l'algoritmo di viterbi
"""
def transition_probability(treebank,states):
    
    prob  = {(tag1, tag2): 0 for tag1 in states for tag2 in states}
    prob_occ = {x: 0 for x in states}
    for phrase in treebank:
        for i in range(1, len(list(phrase))):
            if phrase[i]['upostag'] == "_": continue
            if phrase[i-1]['upostag'] == "_": continue
            previousTag = phrase[i - 1]['upostag']
            currentTag = phrase[i]['upostag']
            prob[previousTag, currentTag] += 1
            prob_occ[previousTag] += 1
    
    for (i, j) in prob:
        prob[(i, j)] /= prob_occ[i]
    
    return prob

"""
Effettua uno smoothing delle osservazioni non conosciute
"""
def smoothing(obs,emission_probability,states):
    # applico lo smoothing sulle osservazioni sconosciute
    for o in obs:
        known = any((s,o) in emission_probability for s in states)
        if not known:
            for s in states:
                emission_probability[(s,o)] = 1/len(states)                
    return emission_probability

"""
Computa le occorrenze di ogni coppia word-tag per l'algoritmo di baseline
"""
def maxTag(treebank):
    words_tags={}
                
    #calcolo delle occorrenze maggiori
    for phrase in treebank:
        for word in phrase:    
            
            form = word["form"]
            tag = word["upostag"]
            
            if tag == "_": continue
        
            if (form,tag) not in words_tags:
                words_tags[(form,tag)] = 1
            else:
                words_tags[(form,tag)] += 1
    return words_tags

"""
Implementa l'algoritmo di Viterbi
"""
def viterbi(obs,emission_probability,transition_probability,start_probability,states):
    
    viterbi = [{}]
    backpointer = {}

    for state in states:
        viterbi[0][state] = start_probability[state] * emission_probability[(state,obs[0])]
        backpointer[state] = [state]

    for i in range(1,len(obs)):
        
        viterbi.append({})
        new_path = {}

        for state in states:

            (probability, possible_state) = max([(viterbi[i-1][s2] 
            * transition_probability[(s2,state)] 
            * emission_probability[(state,obs[i])],s2) for s2 in states])

            viterbi[i][state] = probability
            new_path[state] = backpointer[possible_state] + [state]

        backpointer = new_path

    (probability, state) = max([(viterbi[len(obs) - 1][state], state) for state in states])
    
    return (viterbi, backpointer[state])


"""
Implementa l'algoritmo Baseline
Ipotesi: se due tag hanno egual numerosità si prende il primo
"""
def baseline(obs,states,words_tags):

    tags =[]
                   
    for x in obs:
        known = any((x,y) in words_tags for y in states)
        if not known: 
            tags.append("NOUN")
        try:
            #Valore max di occorrenza
            max_val = max(words_tags[(x,y)] for y in states if (x,y) in words_tags)
            #Ricerca prima posizione max occorenza
            for y in states:
                if (x,y) in words_tags:
                    if max_val == words_tags[(x,y)]:
                        tags.append(y)
                        break
        except ValueError: continue
    return tags
    
    
    