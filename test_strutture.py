# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 16:59:06 2018

@author: Dennis
"""

import pos_tagger as p
import os
from conllu import parse

def viterbi(obs,emission_probability,transition_probability,start_probability,states):
    
    viterbi = [{}]
    backpointer = {}

    for state in states:
        viterbi[0][state] = start_probability[state] * emission_probability[(state,obs[0])]
        backpointer[state] = [state]

    for i in range(1,len(obs)):
        
        viterbi.append({})
        new_path = {}

        for state in states:

            (probability, possible_state) = max([(viterbi[i-1][s2] 
            * transition_probability[(s2,state)] 
            * emission_probability[(state,obs[i])],s2) for s2 in states])

            viterbi[i][state] = probability
            new_path[state] = backpointer[possible_state] + [state]

        backpointer = new_path

    (probability, state) = max([(viterbi[len(obs) - 1][state], state) for state in states])
    
    return (viterbi, backpointer[state], backpointer)

path = os.path.dirname(os.path.abspath(__name__))

obs = "Paolo ama Francesca tanto .".split()

with open(path+"/UD_Italian/all-IT-ud21-train.conllu", 'r',encoding="utf8",errors='ignore') as file:
  data = file.read()
ud_train = parse(data)

print("Sto computando le probabilità...")
states = p.searchState(ud_train)
emission_probability = p.emission_probability(ud_train,states)
transition_probability = p.transition_probability(ud_train,states)
start_probability = p.ProbStart(ud_train,states)

print("Sto computando le massime occorrenze...")
words_tags = p.maxTag(ud_train)

ep_smoothed = p.smoothing(obs,emission_probability,states)
viterbi, tag_viterbi, backpointer = viterbi(obs,ep_smoothed,transition_probability,start_probability,states)


