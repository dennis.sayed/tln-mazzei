# Direct Translate ITA-ENG 

Progetto per il corso di Tecnologie del Linguaggio Naturale che ha lo scopo di
effettuare la traduzione ITA-ENG sulla base di 3 frasi date, attraverso lo
sviluppo di un PoS Tagger basato sull'algoritmo di Viterbi.

Viene inoltre valutata l'accuratezza sia di Viterbi sia della Baseline.


## Getting Started

Per l'esecuzione richiede l'intallazione di un ambiente Python 3.6+ e di tali
libreria:
* conllu

```
pip install conllu
```


## Built With

* [Python](https://www.python.org/) - Linguaggio usato per il progetto
* [Anaconda](https://www.anaconda.com/download/) - Environment manager
* [Conluu](https://github.com/EmilStenstrom/conllu) - CoNLL-U Parser per leggere i file treebank

## Authors

* **Dennis Sayed**
* **Federico Perrone**